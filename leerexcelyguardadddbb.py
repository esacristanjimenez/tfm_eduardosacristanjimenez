import pandas as pd
import pymysql
import glob
import os
import numpy as np

# #Proceso de insertar en DDBB
connection = pymysql.connect(host="localhost", user="root", password="eduardo16", db="DatosFutbol")
cursor=connection.cursor()

ruta="C:/Users/MSI/Documents/Master/MiguelFerreira_2811-2911-3011-0703-0803-1105-1605/2023-03-08/partidosLigaJornada0803"
os.chdir(ruta)

extension='xlsx'
archivosExcel = [i for i in glob.glob('*.{}'.format(extension))]

datosPartidos=pd.DataFrame()

for excel in archivosExcel:
    datos=pd.read_excel(excel)
    datosPartidos=pd.concat([datosPartidos, datos])
    print("Excel: ", excel)
    
#Borrar columnas innecesarias
datosPartidos=datosPartidos.drop(['Unnamed: 0','shot','groundDuel','aerialDuel','infraction','carry'], axis=1)
#Cambiar los null por 0
datosPartidos=datosPartidos.replace(np.nan, "0")
#Dividir la columna dateutc en fecha y hora de inicio del partido
datosPartidos[['FechaPartido','HoraPartido']]=datosPartidos['dateutc'].str.split(" ", expand=True)
#Dividir la columna label en partido y resultado
datosPartidos[['Partido','Resultado']]=datosPartidos['label'].str.split(", ", expand=True)
#Unir la fecha del partido con los minutos del evento
datosPartidos['FechaYMinutoPartido']=datosPartidos['FechaPartido'].str.cat(datosPartidos['matchTimestamp'], sep=' ')
#Eliminar columnas que no se usan
datosPartidos=datosPartidos.drop(['label','matchTimestamp','dateutc'], axis=1)
#Ordenar columnas para que coincidan con la DDBB
datosPartidos=datosPartidos[['id','matchId','matchPeriod','minute','second','videoTimestamp','relatedEventId','type.primary','type.secondary',
                             'location.x','location.y','team.id','team.name','team.formation','opponentTeam.id','opponentTeam.name','opponentTeam.formation','player.id','player.name','player.position',
                             'pass.accurate','pass.angle','pass.height','pass.length','pass.recipient.id','pass.recipient.name','pass.recipient.position','pass.endLocation.x','pass.endLocation.y',
                             'possession.id','possession.duration','possession.types','possession.eventsNumber','possession.eventIndex','possession.startLocation.x','possession.startLocation.y',
                             'possession.endLocation.x','possession.endLocation.y','possession.team.id','possession.team.name','possession.team.formation','possession.attack.withShot',
                             'possession.attack.withShotOnGoal','possession.attack.withGoal','possession.attack.flank','possession.attack.xg','pass','aerialDuel.opponent.id','aerialDuel.opponent.name',
                             'aerialDuel.opponent.position','aerialDuel.opponent.height','aerialDuel.firstTouch','aerialDuel.height','aerialDuel.relatedDuelId','groundDuel.opponent.id',
                             'groundDuel.opponent.name','groundDuel.opponent.position','groundDuel.duelType','groundDuel.keptPossession','groundDuel.progressedWithBall','groundDuel.stoppedProgress',
                             'groundDuel.recoveredPossession','groundDuel.takeOn','groundDuel.side','groundDuel.relatedDuelId','carry.progression','carry.endLocation.x','carry.endLocation.y',
                             'possession.attack','infraction.yellowCard','infraction.redCard','infraction.type','infraction.opponent.id','infraction.opponent.name','infraction.opponent.position',
                             'possession','infraction.opponent','shot.bodyPart','shot.isGoal','shot.onTarget','shot.goalZone','shot.xg','shot.postShotXg','shot.goalkeeperActionId','shot.goalkeeper',
                             'shot.goalkeeper.id','shot.goalkeeper.name','location','FechaPartido','HoraPartido','Partido','Resultado','FechaYMinutoPartido']]

#datosPartidos.to_excel('DatosOrdenados.xlsx')
print(datosPartidos)


#Guardar competición en DDBB
#Insert valores en tabla maestrocompeticiones
for index, row in datosPartidos.iterrows():
    cursor.execute("INSERT INTO datosjugadoreswyscout values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                   [row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9],row[10],row[11],row[12],row[13],row[14],row[15],row[16],row[17],row[18],row[19],row[20],row[21],row[22],row[23],row[24],row[25],row[26],row[27],row[28],row[29],row[30],row[31],row[32],row[33],row[34],row[35],row[36],row[37],row[38],row[39],row[40],row[41],row[42],row[43],row[44],row[45],row[46],row[47],
                    row[48],row[49],row[50],row[51],row[52],row[53],row[54],row[55],row[56],row[57],row[58],row[59],row[60],row[61],row[62],row[63],row[64],row[65],row[66],row[67],row[68],row[69],row[70],row[71],row[72],row[73],row[74],row[75],row[76],row[77],row[78],row[79],row[80],row[81],row[82],row[83],row[84],row[85],row[86],row[87],row[88],row[89],row[90],row[91],row[92]])
connection.commit()

# #Guardar y cerrar conexion
connection.close()
