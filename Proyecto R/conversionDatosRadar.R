library(dplyr)
library(tidyr)
library(stringr)

radar <- standardTable %>%
  pivot_longer(
    cols = c(6:59),
    names_to = "Dimension")

radarAux <- defenseTable %>%
  pivot_longer(
    cols = c(5:36),
    names_to = "Dimension")
radar <- merge(x=radar,y=radarAux, all=TRUE)

radarAux <- gcaTable %>%
  pivot_longer(
    cols = c(5:36),
    names_to = "Dimension")
radar <- merge(x=radar,y=radarAux, all=TRUE)

radarAux <- keeperAdvTable %>%
  pivot_longer(
    cols = c(5:54),
    names_to = "Dimension")
radar <- merge(x=radar,y=radarAux, all=TRUE)

radarAux <- keeperTable %>%
  pivot_longer(
    cols = c(6:35),
    names_to = "Dimension")
radar <- merge(x=radar,y=radarAux, all=TRUE)

radarAux <- miscTable %>%
  pivot_longer(
    cols = c(5:37),
    names_to = "Dimension")
radar <- merge(x=radar,y=radarAux, all=TRUE)

radarAux <- passingTable %>%
  pivot_longer(
    cols = c(5:52),
    names_to = "Dimension")
radar <- merge(x=radar,y=radarAux, all=TRUE)

radarAux <- possesionTable %>%
  pivot_longer(
    cols = c(5:48),
    names_to = "Dimension")
radar <- merge(x=radar,y=radarAux, all=TRUE)

radarAux <- shootingTable %>%
  pivot_longer(
    cols = c(5:36),
    names_to = "Dimension")
radar <- merge(x=radar,y=radarAux, all=TRUE)

radar$value = radar$value / 100
radar$Link <- "Link"

#Guardar Tablas en la DDBB
library(RMySQL)

mysqlconnection = dbConnect(RMySQL::MySQL(),
                            dbname='datosFutbol',
                            host='localhost',
                            port=3306,
                            user='root',
                            password='********')


dbSendQuery(mysqlconnection, "SET GLOBAL local_infile = true;") # <--- Added this
dbWriteTable(mysqlconnection, value = radar, row.names = FALSE, name = "datosradar", overwrite = TRUE)
