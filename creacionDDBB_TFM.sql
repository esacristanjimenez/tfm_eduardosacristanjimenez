CREATE DATABASE datosFutbol;

use datosFutbol;
create table model_radar(
	link varchar(255),
    layer varchar(255),
    point varchar(255)
);

insert into model_radar(link, layer, point)
values('Link', 'Radar', 'Actual Radar');
select * from model_radar;

create table grid_model_radar(
	link varchar(255),
    line int
);

insert into grid_model_radar(link, line)
values('Link', 10);
select * from grid_model_radar;


create table datosjugadoreswyscout(
id integer,
matchId integer,
matchPeriod varchar(255),
minute integer,
second integer,
videoTimestamp float,
relatedEventId integer,
typeprimary varchar(255),
typesecondary varchar(255),
locationX integer,
locationY integer,
teamId integer,
teamName varchar(255),
teamFormation varchar(255),
opponentTeamId integer,
opponentTeamName varchar(255),
opponentTeamFormation varchar(255),
playerId integer,
playerName varchar(255),
playerPosition varchar(255),
passAccurate varchar(255),
passAngle float,
passHeight varchar(255),
passLength float,
passRecipientId integer,
passRecipientName varchar(255),
passRecipientPosition varchar(255),
passEndLocationX integer,
passEndLocationY integer,
possessionId integer,
possessionDuration float,
possessionTypes varchar(255),
possessionEventsNumber integer,
possessionEventIndex integer,
possessionStartLocationX integer,
possessionStartLocationY integer,
possessionEndLocationX integer,
possessionEndLocationY integer,
possessionTeamId integer,
possessionTeamName varchar(255),
possessionTeamFormation varchar(255),
possessionAttackWithShot varchar(255),
possessionAttackWithShotOnGoal varchar(255),
possessionAttackWithGoal varchar(255),
possessionAttackFlank varchar(255),
possessionAttackXg varchar(255),
pass varchar(255),
aerialDuelOpponentId integer,
aerialDuelOpponentName varchar(255),
aerialDuelOpponentPosition varchar(255),
aerialDuelOpponentHeight integer,
aerialDuelFirstTouch varchar(255),
aerialDuelHeight integer,
aerialDuelRelatedDuelId integer,
groundDuelOpponentId integer,
groundDuelOpponentName varchar(255),
groundDuelOpponentPosition varchar(255),
groundDuelDuelType varchar(255),
groundDuelKeptPossession varchar(255),
groundDuelProgressedWithBall varchar(255),
groundDuelStoppedProgress varchar(255),
groundDuelRecoveredPossession varchar(255),
groundDuelTakeOn varchar(255),
groundDuelSide varchar(255),
groundDuelRelatedDuelId integer,
carryProgression float,
carryEndLocationX integer,
carryEndLocationY integer,
possessionAttack varchar(255),
infractionYellowCard varchar(255),
infractionRedCard varchar(255),
infractionType varchar(255),
infractionOpponentId integer,
infractionOpponentName varchar(255),
infractionOpponentPosition varchar(255),
possession varchar(255),
infractionOpponent varchar(255),
shotBodyPart varchar(255),
shotIsGoal varchar(255),
shotOnTarget varchar(255),
shotGoalZone varchar(255),
shotXg float,
shotPostShotXg float,
shotGoalkeeperActionId integer,
shotGoalkeeper varchar(255),
shotGoalkeeperId integer,
shotGoalkeeperName varchar(255),
location varchar(255),
fechaPartido date,
horaPartido time,
partido varchar(255),
resultado varchar(255), 
fechaYMinutopartido datetime(3)
);
